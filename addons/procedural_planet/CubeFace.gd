class_name CubeFace

var mesh = MeshInstance.new()
var local_up : Vector3
var axis_a : Vector3
var axis_b : Vector3
var mutator: ShapeMutator

func _init(local_coords: Vector3, shape_mutator: ShapeMutator):
	local_up = local_coords
	mutator = shape_mutator

	axis_a = Vector3(local_up.y, local_up.z, local_up.x)
	axis_b = axis_a.cross(local_up)

func construct_mesh(resolution, material) -> Mesh:
	var verts = []
	var uvs = []
	var triangles = []

	for y in range(resolution):
		for x in range(resolution):
			var ratio = Vector2(x, y) / (resolution - 1)
			var point_on_unit_cube = (
					local_up
					+ 2*axis_a*(ratio.x - 0.5)
					+ 2*axis_b*(ratio.y - 0.5)
			)
			var point_on_unit_sphere = point_on_unit_cube.normalized()
			uvs.append(Vector2(ratio.x, ratio.y))

			var mutated_point = mutator.mutate_point(point_on_unit_sphere)
			verts.append(mutated_point)

	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	st.set_material(material)

	for y in range(resolution - 1):
		for x in range(resolution - 1):
			var vert_index = x + y*resolution

			st.add_uv(uvs[vert_index])
			st.add_vertex(verts[vert_index])
			st.add_uv(uvs[vert_index + resolution + 1])
			st.add_vertex(verts[vert_index + resolution + 1])
			st.add_uv(uvs[vert_index + resolution])
			st.add_vertex(verts[vert_index + resolution])
			
			st.add_uv(uvs[vert_index])
			st.add_vertex(verts[vert_index])
			st.add_uv(uvs[vert_index + 1])
			st.add_vertex(verts[vert_index + 1])
			st.add_uv(uvs[vert_index + resolution + 1])
			st.add_vertex(verts[vert_index + resolution + 1])

	st.index()
	st.generate_normals()
	st.generate_tangents()
	mesh.mesh = st.commit()

	return mesh.mesh
	
