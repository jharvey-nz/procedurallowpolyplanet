tool
extends Spatial

export(int, 2, 256) var resolution setget update_res
export(Material) var material setget update_col
export(float) var size = 1 setget update_size

export(int, 1, 16) var octaves = 4 setget update_octaves
export(float, 0.001, 5) var period = 4 setget update_period
export(float, 0, 1) var persistence = 0.8 setget update_persistence
export(float, 1.5, 2.5) var lacunarity = 2 setget update_lacunarity
export(float, 0, 4) var noise_scale = 1 setget update_noise_scale

const SIDES = [
	Vector3.UP, Vector3.DOWN,
	Vector3.LEFT, Vector3.RIGHT,
	Vector3.FORWARD, Vector3.BACK
]
var meshes = []
var side_faces = []
var mutator

func _init():
	mutator = ShapeMutator.new(size, octaves, period, persistence, lacunarity, noise_scale)

	for side in SIDES:
		var new_side = CubeFace.new(side, mutator)
		add_child(new_side.mesh)
		side_faces.append(new_side)
		
		construct_sphere()

func construct_sphere():
	for ii in range(len(side_faces)):
		side_faces[ii].construct_mesh(resolution, material)

func update_res(new_res):
	resolution = new_res
	construct_sphere()

func update_col(new_mat):
	material = new_mat
	construct_sphere()
	
func update_size(new_size):
	size = new_size
	if mutator != null:
		mutator.size = size
		construct_sphere()

func update_mutator(new_mut):
	mutator = new_mut
	construct_sphere()

func update_octaves(new_val):
	octaves = new_val
	if mutator != null:
		mutator.noise.octaves = new_val
		construct_sphere()

func update_period(new_val):
	period = new_val
	if mutator != null:
		mutator.noise.period = new_val
		construct_sphere()

func update_persistence(new_val):
	persistence = new_val
	if mutator != null:
		mutator.noise.persistence = new_val
		construct_sphere()
		
func update_lacunarity(new_val):
	lacunarity = new_val
	if mutator != null:
		mutator.noise.lacunarity = new_val
		construct_sphere()

func update_noise_scale(new_scale):
	noise_scale = new_scale
	if mutator != null:
		mutator.noise_scale = new_scale
		construct_sphere()
