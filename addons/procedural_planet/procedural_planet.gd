tool
extends EditorPlugin


func _enter_tree():
	add_custom_type("Procedural Planet", "Spatial", preload("CubeSphere.gd"), preload("icon_procedural_planet.svg"))


func _exit_tree():
	remove_custom_type("Procedural Planet")
