class_name ShapeMutator

var size
var noise
var noise_scale

func _init(new_size, octaves, period, persistence, lacunarity, scale):
	noise = OpenSimplexNoise.new()
	# noise.seed = randi()
	size = new_size
	noise.octaves = octaves
	noise.period = period
	noise.persistence = persistence
	noise.lacunarity = lacunarity
	noise_scale = scale

func mutate_point(point: Vector3) -> Vector3:
	var elevation = noise.get_noise_3dv(point)*noise.get_noise_3dv(point)*noise_scale;
	return point * size * (1 + elevation)
