extends CollisionShape

func _ready():
	var planet = get_parent().get_parent()
	set_shape(planet.verts)
