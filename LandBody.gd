extends StaticBody

var collision_shapes = []

func _ready():
	var planet = get_parent()
	for side in planet.side_faces:
		print('setting shape...')
		var shape = CollisionShape.new()
		shape.set_shape(side.mesh.mesh.create_trimesh_shape())
		collision_shapes.append(shape)
		
		add_child(shape)
