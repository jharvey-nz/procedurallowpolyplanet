shader_type spatial;
render_mode blend_mix,diffuse_burley,specular_schlick_ggx;

uniform vec4 AlbedoColor : hint_color; 
uniform float Roughness;

uniform vec2 Direction = vec2(0, 0);
uniform float Steepness = 0.5;
uniform float Wavelength = 1;

void vertex() {
	vec3 p = VERTEX;

	float k = 2.0 * 3.14 / Wavelength;
	float c = sqrt(9.8/k);
	float a = Steepness / k;
	
	vec3 d = vec3(-VERTEX.y, -VERTEX.z, -0);
	float f = k * (dot(d, p.xyz) - c*TIME);
	
	p.x += d.x*(a * cos(f));
	p.y += d.y*(sin(f) * a);
	p.z += d.z*(a * cos(f));
	
	vec3 tangent = normalize(vec3(
			1.0 - k * a * sin(f), 
			k * a * cos(f), 
			0
	));
	VERTEX = p;
}

void fragment() {
	ALBEDO = AlbedoColor.rgb;
	ALPHA = AlbedoColor.a;
	ROUGHNESS = Roughness;
}