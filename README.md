# Procedural Low Poly Planet Generator

Adds an editor addon to the Godot editor, adding low poly terrain and water generation configurable from the normal user interface.

Written in GDScript for the Godot game engine.

See example-editor.png and example.mp4.
